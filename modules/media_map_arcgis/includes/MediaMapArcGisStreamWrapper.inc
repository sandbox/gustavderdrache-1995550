<?php

/**
 * @file
 * Provides the MediaMapArcGisStreamWrapper class.
 */

/**
 * Implements the stream wrapper for ArcGIS maps.
 */
class MediaMapArcGisStreamWrapper extends MediaReadOnlyStreamWrapper {
  /**
   * Returns the MIME type for ArcGIS maps.
   *
   * @return string
   *   The imaginary MIME type application/x-arcgis
   */
  public static function getMimeType($uri, $mapping = NULL) {
    return 'application/x-arcgis';
  }

  /**
   * Returns the target.
   */
  public static function getTarget($f) {
    return FALSE;
  }

  /**
   * Returns the image preview for this map.
   */
  public function getPreviewPath() {
    return $this->getLocalPath();
  }

  /**
   * Returns the path to a generic map preview image.
   *
   * Currently, this just returns a fixed image lovingly hand-drawn by yours
   * truly.
   */
  protected function getLocalPath() {
    return 'public://media-map-preview.png';
  }
}
