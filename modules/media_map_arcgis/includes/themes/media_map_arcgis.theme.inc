<?php
/**
 * @file
 * Theme functions for Media Map.
 */

/**
 * Preprocesses the media_map_arcgis theme.
 */
function media_map_arcgis_preprocess_media_map_arcgis(&$variables) {
  $wrapper = file_stream_wrapper_get_instance_by_uri($variables['uri']);

  $params = $wrapper->get_parameters();

  $type = $params['t'];

  $src = '//www.arcgis.com/home/webmap/';
  $query_params = array('webmap' => $params['i']);

  // The 'viewer' type is the simpler of the two ArcGIS endpoints.
  if ($type === 'viewer') {
    $src .= 'embedViewer.html';

    if (isset($params['e'])) {
      $query_params['extent'] = $params['e'];
    }

    if (isset($params['z']) && $params['z']) {
      $query_params['zoom'] = 'true';
    }

    if (isset($params['s']) && $params['s']) {
      $query_params['scale'] = 'true';
    }
  }
  // The 'embed' type gets all the gadgets.
  else {
    $src .= 'templates/OnePane/basicviewer/embed.html';

    if (isset($params['e'])) {
      $query_params['gcsextent'] = $params['e'];
    }

    if (isset($params['z']) && $params['z']) {
      $query_params['displayslider'] = 'true';
    }

    if (isset($params['s']) && $params['s']) {
      $query_params['displayscalebar'] = 'true';
    }

    if (isset($params['l']) && $params['l']) {
      $query_params['displaylegend'] = 'true';
    }

    if (isset($params['d']) && $params['d']) {
      $query_params['displaydetails'] = 'true';
    }

    if (isset($params['se']) && $params['se']) {
      $query_params['displaysearch'] = 'true';
    }

    if (isset($params['b']) && $params['b']) {
      $query_params['displaybasemaps'] = 'true';
    }
  }

  // Construct all the variables for the theme.
  $variables['mapsrc'] = $src . '?' . http_build_query($query_params, '', '&amp;');
  $variables['width']  = $params['w'];
  $variables['height'] = $params['h'];
}
