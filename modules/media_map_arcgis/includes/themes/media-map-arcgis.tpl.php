<?php
/**
 * @file
 * Displays an ArcGIS map.
 *
 * Available variables:
 * - $width: The width of the iframe.
 * - $height: The height of the iframe.
 * - $mapsrc: The full URL of the map (with parameters).
 */
?><iframe width="<?php print $width; ?>" height="<?php print $height ?>" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="<?php print $mapsrc ?>"></iframe>
