<?php

/**
 * @file
 * Provides an internet handler for ArcGIS map URLs and iframes.
 */

/**
 * Implementation of MediaInternetBaseHandler.
 *
 * @see hook_media_internet_providers()
 */
class MediaInternetMapArcGisHandler extends MediaInternetBaseHandler {
  /**
   * Overrides MediaInternetBaseHandler::claim().
   */
  public function claim($embed_code) {
    return (bool) $this->parse($embed_code);
  }

  /**
   * Parses iframe/url codes from ArcGIS and captures necessary map parameters.
   *
   * Overrides MediaInternetBaseHandler::parse().
   */
  public function parse($embed_code) {
    static $patterns = array(
      'viewer' => '@<iframe width="(?P<width>\d+)" height="(?P<height>\d+)" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https?://www\.arcgis\.com/home/webmap/embedViewer\.html\?webmap=(?P<mapid>[0-9a-f]+)&(?:amp;)?extent=(?P<extent>[^&"]+)(?P<zoom>&(?:amp;)?zoom=true)?(?P<scale>&(?:amp;)?scale=true)?">@',
      'embed' => '@<iframe width="(?P<width>\d+)" height="(?P<height>\d+)" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https?://www\.arcgis\.com/home/webmap/templates/OnePane/basicviewer/embed\.html\?webmap=(?P<mapid>[0-9a-f]+)&(?:amp;)?gcsextent=(?P<extent>[^&"]+)(?P<zoom>&(?:amp;)?displayslider=true)?(?P<scale>&(?:amp;)?displayscalebar=true)?(?P<legend>&(?:amp;)?displaylegend=true)?(?P<details>&(?:amp;)?displaydetails=true)?(?P<search>&(?:amp;)?displaysearch=true)?(?P<base>&(?:amp;)?displaybasemaps=true)?">@i',
    );

    // Does it look like an <iframe> tag we know how to handle?
    foreach ($patterns as $type => $re) {
      if (preg_match($re, $embed_code, $matches)) {
        return self::buildUri($type, $matches);
      }
    }

    // If not, is it a URL we can handle?
    $parsed = parse_url($embed_code);
    if ($parsed !== FALSE) {
      // Make sure we have a properly-parsed URL.
      if (!(isset($parsed['host']) && isset($parsed['path']) && isset($parsed['query']))) {
        return;
      }

      // Is this from the ArcGIS domain?
      if ($parsed['host'] !== 'www.arcgis.com' && $parsed['host'] !== 'arcgis.com') {
        return;
      }

      // Does the URL correspond to a viewer page?
      if ($parsed['path'] !== '/home/webmap/embedViewer.html' && $parsed['path'] !== '/home/webmap/viewer.html') {
        return;
      }

      // The 'webmap' query parameter is required.
      parse_str($parsed['query'], $query);
      if (!isset($query['webmap'])) {
        return;
      }

      $matches = array('mapid' => $query['webmap']);

      // If the 'extent' parameter exists, add it.
      if (isset($query['extent'])) {
        $matches['extent'] = $query['extent'];
      }

      return self::buildUri('viewer', $matches);
    }
  }

  /**
   * Returns a file object representing an embeddable ArcGIS map.
   */
  public function getFileObject() {
    $uri = $this->parse($this->embedCode);

    return file_uri_to_object($uri, TRUE);

  }

  /**
   * Creates a URI from the parsed embed code.
   */
  protected static function buildUri($type, $matches) {
    $params = self::buildParams($type, $matches);

    $uri = 'arcgis://';

    foreach ($params as $key => $value) {
      $uri .= $key . '/' . $value . '/';
    }

    return file_stream_wrapper_uri_normalize($uri);
  }

  /**
   * Produces iframe parameters from a parsed embed code.
   */
  protected static function buildParams($type, array $matches) {
    // Handle the vast array of possible parameters in an ArcGIS map iframe.
    $params = array(
      'i' => $matches['mapid'],
      't' => $type,
      // Default width x height is 500x400 (corresponds to "Medium" map size).
      'h' => isset($matches['height']) ? $matches['height'] : 400,
      'w' => isset($matches['width']) ? $matches['width'] : 500,
    );

    // Below are the parameters common to both embed types.
    if (isset($matches['extent'])) {
      $params['e'] = $matches['extent'];
    }

    if (isset($matches['zoom'])) {
      $params['z'] = 1;
    }

    if (isset($matches['scale'])) {
      $params['s'] = 1;
    }

    // Now handle the more complex embed.html parameters.
    if ($type == 'embed') {
      if (isset($matches['legend'])) {
        $params['l'] = 1;
      }

      if (isset($matches['details'])) {
        $params['d'] = 1;
      }

      if (isset($matches['search'])) {
        $params['se'] = 1;
      }

      if (isset($matches['base'])) {
        $params['b'] = 1;
      }
    }

    return $params;
  }
}
